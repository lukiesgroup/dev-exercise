package com.campusedu.devexercise.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.campusedu.devexercise.models.Course;
import com.campusedu.devexercise.models.CoursesRepository;

// Make a component so it can be wired into the controller
@Component
public class CoursesService {
	
	// Inject CoursesRepository
	@Autowired CoursesRepository repository;
	
	/**
	 * This service function accepts two parameters and runs a specific repository query that 
	 * returns a list of courses based on the values of the parameters.
	 * @param prefix  Course Code Prefix string
	 * @param number  Course Code Number long
	 * @return List<Course>
	 */
	public List<Course> getCourses(String prefix, Long number) {
		
		if (!prefix.equals("") && number !=0) {
			// If both prefix and number are set find all courses with both prefix and number
			return (List<Course>) repository.findAllByCourseCodePrefixAndCourseCodeNumber(prefix.toUpperCase(), number);
		} else if (!prefix.equals("")) {
			// If prefix is set find all courses with matching prefix
			return (List<Course>) repository.findAllByCourseCodePrefix(prefix.toUpperCase());
		} else if (number != 0) {
			// if number is set find all courses with matching number
			return (List<Course>) repository.findAllByCourseCodeNumber(number);
		}
		// if neither number or prefix are set return all courses
		return (List<Course>) repository.findAll();
	}
}
