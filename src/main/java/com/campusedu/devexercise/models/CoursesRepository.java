package com.campusedu.devexercise.models;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * 
 * Annotated repository component extending the spring CrudRepository for specific data handling
 *
 */
@Repository
public interface CoursesRepository extends CrudRepository<Course, Long>{
	List<Course> findAllByCourseCodePrefixAndCourseCodeNumber(String courseCodePrefix, Long courseCodeNumber);
	List<Course> findAllByCourseCodePrefix(String courseCodePrefix);
	List<Course> findAllByCourseCodeNumber(Long courseCodeNumber);
}
