package com.campusedu.devexercise.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.campusedu.devexercise.models.Course;
import com.campusedu.devexercise.services.CoursesService;

// Make and MVC controller that returns an object rather than view
@RestController	
public class CoursesController {
	
	// Inject CourseService
	@Autowired CoursesService service;
	
	/**
	 * Leverages service that retrieves a list of courses based on given parameters and returns a list of courses.
	 * @param pre Course Code Prefix string
	 * @param num Course Code Number long
	 * @return List<Course>
	 */
	@GetMapping("/courses")	//Map GET requests for /courses to courses()
	public List<Course> courses(@RequestParam(name="course_code_prefix", defaultValue="") String pre, 
			@RequestParam(name="course_code_number", defaultValue="0") Long num) {
		
		// Return service response
		return service.getCourses(pre, num);
	}
}
