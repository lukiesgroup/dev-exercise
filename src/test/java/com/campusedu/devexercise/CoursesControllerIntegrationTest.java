package com.campusedu.devexercise;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;

import com.campusedu.devexercise.models.Course;

/**
 * 
 * Test CoursesController.CoursesService logic using live data and SpringBootTest
 *
 */

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CoursesControllerIntegrationTest {

	@Autowired
	private TestRestTemplate testRestTemplate;
	
	@Test
	public void getCoursesWithPrefixAndNumber() {
		ResponseEntity<Course[]> courseResponse = testRestTemplate.getForEntity("/courses?course_code_prefix=art&course_code_number=270", Course[].class);
		
		assertEquals(2, courseResponse.getBody().length);
	}	
	
	@Test
	public void getCoursesWithPrefixOnly() {
		ResponseEntity<Course[]> courseResponse = testRestTemplate.getForEntity("/courses?course_code_prefix=art", Course[].class);
		
		assertEquals(18, courseResponse.getBody().length);
	}
	
	@Test
	public void getCoursesWithNumberOnly() {
		ResponseEntity<Course[]> courseResponse = testRestTemplate.getForEntity("/courses?course_code_number=270", Course[].class);
		
		assertEquals(5, courseResponse.getBody().length);
	}
	
	@Test
	public void getCoursesWithNoFilter() {
		ResponseEntity<Course[]> courseResponse = testRestTemplate.getForEntity("/courses", Course[].class);
		
		assertEquals(548, courseResponse.getBody().length);
	}
}
