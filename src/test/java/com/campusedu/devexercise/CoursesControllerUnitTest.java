package com.campusedu.devexercise;

import java.util.ArrayList;
import java.util.List;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.campusedu.devexercise.controllers.CoursesController;
import com.campusedu.devexercise.models.Course;
import com.campusedu.devexercise.services.CoursesService;

/**
 * 
 * Test CoursesController response using WebMvcTest
 *
 */
@WebMvcTest (CoursesController.class)
public class CoursesControllerUnitTest {
	
	//@Autowired
	//private CoursesController coursesController;
	
	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private CoursesService coursesService;
	
	@Test
	public void getCoursesTest() throws Exception {
		// Mock the data returned by courseService class
		List<Course> courses = new ArrayList<Course>();
		
		Course course1 = new Course();
		course1.setId((long)1);
		course1.setCourseCodePrefix("EDU");
		course1.setCourseCodeNumber((long)320);
		courses.add(course1);
		
		Course course2 = new Course();
		course2.setId((long)2);
		course2.setCourseCodePrefix("EDU");
		course2.setCourseCodeNumber((long)320);
		courses.add(course2);
		
		Course course3 = new Course();
		course3.setId((long)3);
		course3.setCourseCodePrefix("BLU");
		course3.setCourseCodeNumber((long)400);
		courses.add(course3);
		
		Course course4 = new Course();
		course4.setId((long)4);
		course4.setCourseCodePrefix("EDU");
		course4.setCourseCodeNumber((long)100);
		courses.add(course4);
		
		
		Mockito.when(coursesService.getCourses(Mockito.anyString(), Mockito.anyLong())).thenReturn(courses);
		
		// Make call to courses controller, and make sure we get correct data back.
		mockMvc.perform(MockMvcRequestBuilders.get("/courses")).andExpect(MockMvcResultMatchers.status().isOk())
		.andExpect(MockMvcResultMatchers.content().contentType("application/json"))
		.andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.hasSize(4)));
		
	}
}
